<?php

namespace Ow\MarketingApi\Kuaishou\Material;

use Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException;
use Ow\MarketingApi\Kuaishou\Kernel\Http\BaseHttpClient;

class Video extends BaseHttpClient
{

    /**
     * @param array $data
     * @return array
     * @throws ValidateRequestParamException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @see https://developers.e.kuaishou.com/docs/5.2.2
     */
    public function upload(array $data) : array
    {

        $this->validateRequiredParams($data,[
            'advertiser_id',
            'file',
            'signature',
        ]);

        if ( !is_resource($data['file']) ) throw new ValidateRequestParamException('file must be resource');

        return $this->multipartRequest('rest/openapi/v2/file/ad/video/upload',$data);
    }

    /**
     * 获取视频信息get接口（可通过photo_ids查询所有视频）
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws ValidateRequestParamException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @see https://developers.e.kuaishou.com/docs/5.2.3
     */
    public function detail(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'photo_ids',
        ]);

        $data['photo_ids'] = (array)$data['photo_ids'];

        return $this->httpJsonPost('rest/openapi/v1/file/ad/video/get',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws ValidateRequestParamException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @see https://developers.e.kuaishou.com/docs/5.2.4
     */
    public function lists(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/file/ad/video/list',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws ValidateRequestParamException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @see https://developers.e.kuaishou.com/docs/5.3.1
     */
    public function share(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'photo_ids',
            'share_advertiser_ids',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/file/ad/video/share',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws ValidateRequestParamException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @see https://developers.e.kuaishou.com/docs/5.3.2
     */
    public function update(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'photo_ids',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/file/ad/video/update',$data);
    }


    /**
     * @param array $data
     * @return array
     * @throws ValidateRequestParamException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @see https://developers.e.kuaishou.com/docs/5.3.3
     */
    public function tagDelete(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'photo_ids',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/file/ad/video/tag/delete',$data);
    }

}