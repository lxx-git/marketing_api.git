<?php

namespace Ow\MarketingApi\Kuaishou\Creative;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CreativeServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        !isset($pimple['creative']) && $pimple['creative'] = function ($app){
            return new Creative($app);
        };

        !isset($pimple['program']) && $pimple['program'] = function ($app){
            return new Program($app);
        };
    }

}