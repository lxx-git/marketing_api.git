<?php

namespace Ow\MarketingApi\Kuaishou\Campaign;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CampaignServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        !isset($pimple['campaign']) && $pimple['campaign'] = function ($app) {
            return new Campaign($app);
        };
    }
}