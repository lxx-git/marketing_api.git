<?php

namespace Ow\MarketingApi\Kuaishou;

use Ow\MarketingApi\Kuaishou\Account\AccountServiceProvider;
use Ow\MarketingApi\Kuaishou\Auth\AccessTokenServiceProvider;
use Ow\MarketingApi\Kuaishou\Campaign\CampaignServiceProvider;
use Ow\MarketingApi\Kuaishou\Creative\CreativeServiceProvider;
use Ow\MarketingApi\Kuaishou\Kernel\ServiceContainer;
use Ow\MarketingApi\Kuaishou\Material\MaterialServiceProvider;
use Ow\MarketingApi\Kuaishou\Plan\PlanServiceProvider;
use Ow\MarketingApi\Kuaishou\Report\ReportServiceProvider;


/**
 * Class Application
 * @property \Ow\MarketingApi\Kuaishou\Account\Advertiser $advertiser
 * @property \Ow\MarketingApi\Kuaishou\Auth\AccessToken $access_token
 * @property \Ow\MarketingApi\Kuaishou\Kernel\Cache\Cache $cache
 * @property \Ow\MarketingApi\Kuaishou\Campaign\Campaign $campaign
 * @property \Ow\MarketingApi\Kuaishou\Creative\Creative $creative
 * @property \Ow\MarketingApi\Kuaishou\Creative\Program $program
 * @property \Ow\MarketingApi\Kuaishou\Plan\Plan $plan
 * @property \Ow\MarketingApi\Kuaishou\Report\Report $report
 * @property \Ow\MarketingApi\Kuaishou\Material\Material $material
 * @property \Ow\MarketingApi\Kuaishou\Material\Video $video
 * @property \Ow\MarketingApi\Kuaishou\Material\Image $image
 * @package Ow\MarketingApi\Kuaishou
 */
class Application extends ServiceContainer
{

    protected $providers = [
        AccessTokenServiceProvider::class,
        AccountServiceProvider::class,
        CampaignServiceProvider::class,
        PlanServiceProvider::class,
        CreativeServiceProvider::class,
        ReportServiceProvider::class,
        MaterialServiceProvider::class,
    ];

    public function __get($name)
    {
        return $this->offsetGet($name);
    }

}