<?php

namespace Ow\MarketingApi\Kuaishou\Plan;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class PlanServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        !isset($pimple['plan']) && $pimple['plan'] = function ($app){
            return new Plan($app);
        };
    }
}