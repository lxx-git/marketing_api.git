<?php

namespace Ow\MarketingApi\Kuaishou\Kernel;


class Config
{

    //可以实现ArrayAccess等接口，成为 Collection

    protected $items = [];

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    public function get($key,$default=null)
    {
        return $this->exists($key) ? $this->items[$key] : $default;
    }

    public function exists($key) : bool
    {
        return array_key_exists($key,$this->items);
    }

    public function forgot($key)
    {
        if ( $this->exists($key) ) unset($this->items[$key]);
    }

    public function set($key,$value)
    {
        $this->items[$key] = $value;
    }

    public function all() : array
    {
        return $this->items;
    }

    public function toArray() : array
    {
        return $this->items;
    }

}