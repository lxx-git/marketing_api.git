<?php


namespace Ow\MarketingApi\Kuaishou\Kernel\Traits;


trait RequestSandbox
{

    protected $sandbox = false;

    public function useSandbox($sandbox=true) : self
    {
        $this->sandbox = $sandbox;
        return $this;
    }

    public function isSandbox() : bool
    {
        return $this->sandbox;
    }

    public function getHost()
    {
        return $this->isSandbox() ? $this->getSandBoxHost() : $this->getProductHost();
    }

    public function getSandBoxHost()
    {
        return 'https://sandbox-ad.e.kuaishou.com/';
    }

    public function getProductHost()
    {
        return 'https://ad.e.kuaishou.com/';
    }

}