<?php

namespace Ow\MarketingApi\Kuaishou\Report;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ReportServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        !isset($pimple['report']) && $pimple['report'] = function ($app){
            return new Report($app);
        };
    }

}