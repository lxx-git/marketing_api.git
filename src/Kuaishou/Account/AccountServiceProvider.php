<?php

namespace Ow\MarketingApi\Kuaishou\Account;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class AccountServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        !isset($pimple['advertiser']) && $pimple['advertiser'] = function ($app){
            return new Advertiser($app);
        };
    }
}