<?php

namespace Ow\MarketingApi\Helper;

class Arr
{

    /**
     * @param array $arr
     * @param array $keys
     * @return bool
     */
    public static function keys_all_exists(array $arr,array $keys) : bool
    {
        foreach ($keys as $key)
        {
            if ( !key_exists($key,$arr) ) return false;
        }

        return true;
    }
}