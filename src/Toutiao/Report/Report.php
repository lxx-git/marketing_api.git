<?php


namespace Ow\MarketingApi\Toutiao\Report;


use Ow\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{
    /**
     * 广告主数据
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function advertiser(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'start_date',
            'end_date'
        ]);
        return $this->httpJsonGet('open_api/2/report/advertiser/get/',$data);
    }

    /**
     * 广告组数据
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function campaign(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'start_date',
            'end_date'
        ]);
        return $this->httpJsonGet('open_api/2/report/campaign/get/',$data);
    }

    /**
     * 获取广告计划数据
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function plan(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'start_date',
            'end_date'
        ]);
        return $this->httpJsonGet('open_api/2/report/ad/get/',$data);
    }

    /**
     * 获取创意数据
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function creative(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'start_date',
            'end_date'
        ]);
        return $this->httpJsonGet('open_api/2/report/creative/get/',$data);
    }

    /**
     * 分级模糊数据
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function misty(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'start_date',
            'end_date',
            'group_by'
        ]);
        return $this->httpJsonGet('open_api/2/report/misty/get/',$data);
    }

    /**
     * 视频素材数据
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function video(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'start_date',
            'end_date'
        ]);
        return $this->httpJsonGet('open_api/2/report/video/get/',$data);
    }

    /**
     * 多合一报表接口
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function integrated(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'start_date',
            'end_date',
            'group_by'
        ]);
        return $this->httpJsonGet('open_api/2/report/integrated/get/',$data);
    }
}