<?php


namespace Ow\MarketingApi\Toutiao\Material;


use Ow\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Video extends BaseHttpClient
{
    /**
     * 上传视频
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function upload(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'video_signature',
            'video_file'
        ]);
        return $this->multipartRequest('open_api/2/file/video/ad/',$data);
    }

    /**
     * 获取视频素材列表
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function list(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id'
        ]);

        return $this->httpJsonGet('open_api/2/file/video/get/',$data);
    }

    /**
     * 智能获取视频封面
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function videoCover(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'video_id'
        ]);

        return $this->httpJsonGet('open_api/2/tools/video_cover/suggest/',$data);
    }

    /**
     * 获取同主体下广告主视频素材
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function advertiserVideo(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'video_ids'
        ]);

        return $this->httpJsonGet('open_api/2/file/video/ad/get/',$data);
    }

    /**
     * 批量删除视频
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function delete(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'video_ids'
        ]);
        return $this->httpJsonPost('open_api/2/file/video/delete/',$data);
    }

    /**
     * 修改视频素材名字
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function update(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'videos'
        ]);
        return $this->httpJsonPost('open_api/2/file/video/update/',$data);
    }
}