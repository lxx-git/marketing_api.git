<?php


namespace Ow\MarketingApi\Toutiao\Material;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
class MaterialServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['material']) && $pimple['material'] = function ($app){
            return new Material($app);
        };
        !isset($pimple['image']) && $pimple['image'] = function ($app){
            return new Image($app);
        };
        !isset($pimple['video']) && $pimple['video'] = function ($app){
            return new Video($app);
        };
    }
}