<?php


namespace Ow\MarketingApi\Toutiao\Material;


use Ow\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Material extends BaseHttpClient
{
    /**
     * 素材推送
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function materialBind(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'target_advertiser_ids'
        ]);

        return $this->httpJsonPost('open_api/2/file/material/bind/',$data);
    }
}