<?php


namespace Ow\MarketingApi\Toutiao\Tools;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ToolsServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        !isset($pimple['tools']) && $pimple['tools'] = function ($app){
            return new Tools($app);
        };
    }
}