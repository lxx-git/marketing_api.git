<?php


namespace Ow\MarketingApi\Toutiao\Account;


use Ow\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Advertiser extends BaseHttpClient
{

    /**
     * 查询账号余额
     * @param $advertiserId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fund($advertiserId)
    {
        return $this->httpJsonGet('open_api/2/advertiser/fund/get/',[
                    'advertiser_id' => $advertiserId
                ]);
    }

    /**
     *查询账号当日的资金流水
     * @param $advertiserId
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dailyStat($advertiserId)
    {
        return $this->httpJsonGet('open_api/2/advertiser/fund/daily_stat/',[
            'advertiser_id' => $advertiserId
        ]);
    }
}