<?php


namespace Ow\MarketingApi\Toutiao\Kernel\Cache;


class Cache
{
    /**
     * @var mixed 只要能调用redis命令的
     */
    protected $driver;

    public function set($key,$value,$exp)
    {
        $this->driver->setex($key,$exp,$value);
    }

    public function get($key)
    {
        return $this->driver->get($key);
    }

    public function forever($key,$value)
    {
        $this->driver->set($key,$value);
    }

    /**
     * @param mixed $driver
     */
    public function setDriver($driver): void
    {
        $this->driver = $driver;
    }

    public function has($key)
    {
        return $this->driver->exists($key);
    }

    public function __call($name,$params)
    {
        return $this->driver->{$name}(...$params);
    }
}