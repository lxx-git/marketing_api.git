<?php


namespace Ow\MarketingApi\Toutiao\Kernel\Providers;


use GuzzleHttp\Client;
use Ow\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;
use Pimple\Container;
use Pimple\ServiceProviderInterface;


class HttpServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['http_client']) && $pimple['http_client'] = function ($app){
            return new Client($app['config']->get('http',[]));
        };
        !isset($pimple['base_client']) && $pimple['base_client'] = function ($app){
            return new BaseHttpClient($app);
        };
    }
}