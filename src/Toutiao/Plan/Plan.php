<?php


namespace Ow\MarketingApi\Toutiao\Plan;


use Ow\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Plan extends BaseHttpClient
{
    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function list(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id'
        ]);

        return $this->httpJsonGet('open_api/2/ad/get/',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function create(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'campaign_id',
            'name',
            'delivery_range',
            'smart_bid_type',
            'flow_control_mode',
            'budget_mode',
            'budget',
            'schedule_type',
            'pricing',
        ]);

        return $this->httpJsonPost('open_api/2/ad/create/',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function update(array $data) :array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_id'
        ]);

        return $this->httpJsonPost('open_api/2/ad/update/',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function updateStatus(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_ids',
            'opt_status'
        ]);
        return $this->httpJsonPost('open_api/2/ad/update/status/',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function updateBudget(array $data) : array
    {
        $this->validateRequiredParams($data ,[
            'advertiser_id',
            'data'
        ]);
        return $this->httpJsonPost('open_api/2/ad/update/budget/',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function updateBid(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'data'
        ]);

        return $this->httpJsonPost('open_api/2/ad/update/bid/',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function rejectReason(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_ids'
        ]);

        return $this->httpJsonGet('open_api/2/ad/reject_reason/',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function costProtectStatus(array $data) :array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_ids'
        ]);

        return $this->httpJsonGet('open_api/2/ad/cost_protect_status/get/',$data);

    }

}