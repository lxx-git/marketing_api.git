<?php

namespace Ow\MarketingApi\BaiDu\Material;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class MaterialServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        !isset($pimple['image']) && $pimple['image'] = function ($app){
            return new Image($app);
        };

        !isset($pimple['video']) && $pimple['video'] = function ($app){
            return new Video($app);
        };
    }
}