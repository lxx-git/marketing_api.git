<?php


namespace Ow\MarketingApi\BaiDu\Material;

use Ow\MarketingApi\BaiDu\Kernel\Http\BaseHttpClient;
class video extends BaseHttpClient
{

    /**上传视频
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function uploadVideo(array $data)
    {

        return $this->multipart("/VideoUploadService/addVideo",$data);
    }

}