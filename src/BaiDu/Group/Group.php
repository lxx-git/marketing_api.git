<?php
namespace Ow\MarketingApi\BaiDu\Group;

use Ow\MarketingApi\BaiDu\Kernel\Http\BaseHttpClient;

class Group extends BaseHttpClient
{
    /**
     * 获取广告单元
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getGroupFeed(array $data)
    {
        return $this->httpJsonPost("/AdgroupFeedService/getAdgroupFeed",$data);
    }

    /**
     * 创建广告单元
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createAdGroup(array $data)
    {
        return $this->httpJsonPost("/AdgroupFeedService/addAdgroupFeed",$data);
    }

}