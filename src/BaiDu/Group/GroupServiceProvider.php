<?php

namespace Ow\MarketingApi\BaiDu\Group;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class GroupServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        !isset($pimple['group']) && $pimple['group'] = function ($app){
            return new Group($app);
        };
    }
}