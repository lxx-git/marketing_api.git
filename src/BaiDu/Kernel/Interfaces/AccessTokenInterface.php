<?php


namespace Ow\MarketingApi\BaiDu\Kernel\Interfaces;


interface AccessTokenInterface
{

    public function getToken();

    public function refresh() : self;

}