<?php


namespace Ow\MarketingApi\BaiDu\Kernel\Traits;


trait RequestSandbox
{

    protected $sandbox = true;

    public function useSandbox($sandbox=true) : self
    {
        $this->sandbox = $sandbox;
        return $this;
    }

    public function isSandbox() : bool
    {
        return $this->sandbox;
    }

    public function getHost()
    {
        return $this->isSandbox() ? $this->getSandBoxHost() : $this->getProductHost();
    }

    public function getOption($options){

       return $this->app['config']->get('type') == 'video' ? $options : array_merge(['header' => $this->app['config']->get('header')],$options);
    }
    public function getSandBoxHost()
    {
        return 'https://api.baidu.com/json/feed/'.$this->app->getConfig()['http']['version'];
    }

    public function getProductHost()
    {
        return 'https://api.baidu.com/json/sms/service';
    }

}