<?php


namespace Ow\MarketingApi\BaiDu\Kernel\Traits;


use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;

trait HasHttpRequest
{

    /**
     * @var \GuzzleHttp\ClientInterface
     */
    protected $httpClient;

    protected $middlewares = [];

    protected $handlerStack ;

//    protected static $defaults = [];

    /**
     * @param string $uri
     * @param array $json
     * @param string $method
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpJson(string $uri,array $json=[],string $method='POST')
    {
        $json = $this->getOption($json);
        return $this->request($method,$uri,['json' => $json]);
    }

    /**
     * @param string $uri
     * @param array $json
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpJsonPost(string $uri,array $json=[])
    {
        return $this->httpJson($uri,$json,'POST');
    }

    /**
     * @param string $uri
     * @param array $json
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpJsonGet(string $uri,array $json=[])
    {
        return $this->httpJson($uri,$json,'GET');
    }

    /**
     * @param $method
     * @param $url
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($method,$url,array $options=[])
    {
        $res =  $this->getHttpClient()->request($method,$url,$options);

        if ( !$res ) throw new \Exception('request fail,no response return');

        return $res;
    }

    /**
     * @param $url
     * @param $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function multipart($url,$data)
    {

        $item = [];
        foreach ($data as $k=>$v){
            if ( is_string($v) || is_numeric($v) || is_resource($v)){
                $item[] = [
                    'name' => $k,
                    'contents' => $v,
                ];
            }
        }
        $options = [
            "multipart" => $item
        ];
        return $this->request("POST",$url,$options);
    }

    /**
     * @return Client|ClientInterface|mixed
     */
    public function getHttpClient()
    {
        return new Client();
    }


    /**
     * Add a middleware.
     *
     * @param string $name
     *
     * @return $this
     */
    public function pushMiddleware(callable $middleware, string $name = null)
    {
        if (!is_null($name)) {
            $this->middlewares[$name] = $middleware;
        } else {
            array_push($this->middlewares, $middleware);
        }

        return $this;
    }

    /**
     * Build a handler stack.
     */
    public function getHandlerStack(): HandlerStack
    {
        if ($this->handlerStack) {
            return $this->handlerStack;
        }

        $this->handlerStack = HandlerStack::create($this->getGuzzleHandler());

        foreach ($this->middlewares as $name => $middleware) {
            $this->handlerStack->push($middleware, $name);
        }

        return $this->handlerStack;
    }

    /**
     * Get guzzle handler.
     *
     * @return callable
     */
    protected function getGuzzleHandler()
    {
        if (property_exists($this, 'app') && isset($this->app['guzzle_handler'])) {
            return is_string($handler = $this->app->raw('guzzle_handler'))
                ? new $handler()
                : $handler;
        }

        return \GuzzleHttp\choose_handler();
    }
}