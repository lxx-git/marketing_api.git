<?php

namespace Ow\MarketingApi\BaiDu\Kernel;

use Ow\MarketingApi\BaiDu\Kernel\Cache\CacheServiceProvider;
use Ow\MarketingApi\BaiDu\Kernel\Cache\GroupServiceProvider;
use Ow\MarketingApi\BaiDu\Kernel\Providers\ConfigServiceProvider;
use Ow\MarketingApi\BaiDu\Kernel\Providers\HttpClientServiceProvider;
//use Ow\MarketingApi\BaiDu\Kernel\Providers\RequestServiceProvider;
use Pimple\Container;

class ServiceContainer extends Container
{

    protected $defaultConfig = [];

    protected $userConfig = [];

    protected $providers = [];

    public function __construct($config = [],array $values = [])
    {
        $this->userConfig = $config;

        parent::__construct($values);

        $this->registerProviders($this->getProviders());
    }

    public function getConfig() : array
    {

        $base = [
            'http' => [
                'timeout' => 30,
                'base_uri' => 'https://api.baidu.com/json/feed/',
                'version' => 'v1',
            ],

        ];

        return array_replace_recursive($base,$this->defaultConfig,$this->userConfig);
    }

    public function getProviders() : array
    {
        return array_merge([
            ConfigServiceProvider::class,
            HttpClientServiceProvider::class,
            CacheServiceProvider::class,
        ],$this->providers);
    }


    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider) {
            parent::register(new $provider());
        }
    }
}