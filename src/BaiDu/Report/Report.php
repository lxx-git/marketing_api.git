<?php

namespace Ow\MarketingApi\BaiDu\Report;

use Ow\MarketingApi\BaiDu\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{
    /**
     * 账户列表
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\BaiDu\Kernel\Exceptions\ValidateRequestParamException
     */
    public function userLists(array $data)
    {
        return $this->httpJsonPost('/MccFeedService/getUserListByMccid',$data);
    }

    /**
     * 获取账户信息
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function userInfo(array $data){

        return $this->httpJsonPost('/AccountFeedService/getAccountFeed',$data);
    }

    /**
     * 获取app列表
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
}