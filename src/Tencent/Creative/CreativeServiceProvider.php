<?php

namespace Ow\MarketingApi\Tencent\Creative;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CreativeServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple['creative'] = function ($app) {
            return new Creative($app);
        };
    }
}