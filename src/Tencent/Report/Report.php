<?php

namespace Ow\MarketingApi\Tencent\Report;

use Ow\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{
    /**
     * 广点通广告计划报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return array|string
     */
    public function plan(array $parameters = [])
    {
        return $this->request("GET", "campaigns/get", $parameters);
    }

    /**
     * 广点通广告组报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param $parameters
     * @return array|string
     */
    public function campaign(array $parameters = [])
    {
        return $this->request("GET", "adgroups/get", $parameters);
    }

    /**
     * 广点通广告报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param $parameters
     * @return array|string
     */
    public function ad(array $parameters = [])
    {
        return $this->request("GET", "ads/get", $parameters);
    }
}