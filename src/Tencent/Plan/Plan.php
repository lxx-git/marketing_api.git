<?php

namespace Ow\MarketingApi\Tencent\Plan;

use Ow\MarketingApi\Tencent\CommonActions\Operate;

class Plan extends Operate
{
    protected $interface = "adgroups";
}