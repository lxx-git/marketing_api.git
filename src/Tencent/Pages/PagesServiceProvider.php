<?php

namespace Ow\MarketingApi\Tencent\Pages;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class PagesServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple["pages"] = function ($app) {
          return new Pages($app);
        };
    }
}
