<?php

namespace Ow\MarketingApi\Tencent\Oauth;

use Ow\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;
use Ow\MarketingApi\Tencent\Kernel\ServiceContainer;

class Oauth extends BaseHttpClient
{
    public function __construct(ServiceContainer $app, string $accessToken)
    {
        parent::__construct($app, $accessToken);
    }

    public function oauthAuthorize($clientId, $redirectUri, $state = null, $scope = null,
                                   $accountType = null, $accountDisplayNumber = null, $fields = null)
    {
        return "get auth_code";
    }
}