<?php

namespace Ow\MarketingApi\Tencent\Kernel\Interfaces;

use Closure;

interface MiddlewareInterface
{
    /**
     * Handle middleware
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next);
}