<?php

namespace Ow\MarketingApi\Tencent\Kernel\Cache;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CacheServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple['cache'] = function ($app) {
            return new Cache($app);
        };
    }
}