<?php

namespace Ow\MarketingApi\Tencent\Kernel;

class BaseObject
{
    /**
     * @var array
     */
    public static $instances = [];

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if (empty(static::$instances[static::class])) {
            static::$instances[static::class] = new static();
        }

        return static::$instances[static::class];
    }
}