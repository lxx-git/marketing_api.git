<?php

namespace Ow\MarketingApi\Tencent\Kernel\MiddlewareModel;

use Closure;
use Ow\MarketingApi\Tencent\Kernel\Interfaces\MiddlewareInterface;

abstract class MiddlewareRequest implements MiddlewareInterface
{

    /**
     * @inheritDoc
     */
    public function handle($request, Closure $next)
    {
        $basicParameters = [
            "nonce" => md5(uniqid("zs_lee_gdt", true)),
            "timestamp" => time(),
            "access_token" => $request['access_token'],
        ];
        return $next($basicParameters);
    }
}


