<?php

namespace Ow\MarketingApi\Tencent\Material;

use Ow\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;

class Image extends BaseHttpClient
{
    /**
     * 上传图片
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return array|string
     */
    public function upload(array $parameters = [])
    {
        return $this->multipartUpload($this->verificationRequestUri("POST", $parameters, "images/add"), $parameters);
    }

    /**
     * 图片列表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function get(array $parameters = [])
    {
        return $this->request("GET", "images/get", $parameters);
    }
}
