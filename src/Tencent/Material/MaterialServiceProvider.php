<?php

namespace Ow\MarketingApi\Tencent\Material;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class MaterialServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple['image'] = function ($app) {
            return new Image($app);
        };

        $pimple['brand'] = function ($app) {
            return new Brand($app);
        };

        $pimple['video'] = function ($app) {
            return new Video($app);
        };
    }
}