<?php

namespace Ow\MarketingApi\Tencent\Material;

use Ow\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;

class Video extends BaseHttpClient
{
    /**
     * 视频上传
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return mixed
     */
    public function upload(array $parameters = [])
    {
        return $this->multipartUpload($this->verificationRequestUri("POST", $parameters, "videos/add"), $parameters);
    }

    /**
     * 视频列表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function get(array $parameters = [])
    {
        return $this->request("GET", "videos/get", $parameters);
    }
}
