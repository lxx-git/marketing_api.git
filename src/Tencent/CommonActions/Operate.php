<?php

namespace Ow\MarketingApi\Tencent\CommonActions;

use Ow\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;

class Operate extends BaseHttpClient
{
    protected $interface;

    /**
     * 新建
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return array|string
     */
    public function create(array $parameters = [])
    {
        return $this->request("POST", "{$this->interface}/add", $parameters);
    }

    /**
     * 修改
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return array|string
     */
    public function update(array $parameters = [])
    {
        return $this->request("POST","{$this->interface}/update", $parameters);
    }

    /**
     * 删除
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return array|string
     */
    public function delete(array $parameters = [])
    {
        return $this->request("POST", "{$this->interface}/delete", $parameters);
    }
}