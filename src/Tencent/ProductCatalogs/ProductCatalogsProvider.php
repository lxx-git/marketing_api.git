<?php

namespace Ow\MarketingApi\Tencent\ProductCatalogs;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ProductCatalogsProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple["product_catalogs"] = function ($app) {
            return new ProductCatalogs($app);
        };
    }
}