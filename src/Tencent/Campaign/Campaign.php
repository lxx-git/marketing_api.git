<?php

namespace Ow\MarketingApi\Tencent\Campaign;

use Ow\MarketingApi\Tencent\CommonActions\Operate;

class Campaign extends Operate
{
    protected $interface = "campaigns";
}