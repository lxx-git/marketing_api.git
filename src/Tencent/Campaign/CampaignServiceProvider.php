<?php

namespace Ow\MarketingApi\Tencent\Campaign;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class CampaignServiceProvider
 * @package Ow\MarketingApi\Tencent\Campaign
 */
class CampaignServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple['campaign'] = function ($app) {
            return new Campaign($app);
        };
    }
}