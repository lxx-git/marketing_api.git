<?php

namespace Ow\MarketingApi;


use Ow\MarketingApi\Kuaishou\Application as Kuaishou;
use Ow\MarketingApi\BaiDu\Application as Baidu;
use Ow\MarketingApi\Tencent\Application as Tencent;
use Ow\MarketingApi\Toutiao\Application as Toutiao;
use Ow\MarketingApi\Vivo\Application as Vivo;
use Ow\MarketingApi\Huawei\Application as Huawei;
use Ow\MarketingApi\Oppo\Application as Oppo;

class Factory
{


    /**
     * 快手
     * @param array $config
     * @return Kuaishou
     */
    public static function Kuaishou(array $config)
    {

        return new Kuaishou($config);
    }

    /**
     * 广点通，腾讯
     * @param array $config
     * @return Tencent
     */
    public static function tencent(array $config)
    {
        return new Tencent($config);
    }

    /**
     * 巨量引擎 ，头条
     * @param array $config
     */
    public static function oceanEngine(array $config)
    {
        return new Toutiao($config);
    }

    /** 百度
     * @param array $config
     * @return Baidu
     */
    public static function BaiDu(array $config)
    {
        return new Baidu($config);
    }

    /**
     *  vivo
     * @param array $config
     * @return Vivo
     */
    public static function Vivo(array $config)
    {
        return new Vivo($config);
    }

    /**
     * huawei
     * @param array $config
     * @return Huawei
     */
    public static function Huawei(array $config)
    {
        return new Huawei($config);
    }

    /**
     * oppo
     * @param array $config
     * @return Oppo
     */
    public static function Oppo(array $config)
    {
        return new Oppo($config);
    }

}