<?php

namespace Ow\MarketingApi\Vivo;

use Ow\MarketingApi\Vivo\Kernel\Cache\CacheServiceProvider;
use Ow\MarketingApi\Vivo\Kernel\ServiceContainer;
use Ow\MarketingApi\Vivo\Report\ReportServiceProvider;

/**
 * Class Application
 * @property \Ow\MarketingApi\Vivo\Report\Report $report
 * @property \Ow\MarketingApi\Vivo\Kernel\Cache\Cache $cache
 * @package Ow\MarketingApi\Vivo
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        ReportServiceProvider::class,
        CacheServiceProvider::class,
    ];
}
