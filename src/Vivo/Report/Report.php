<?php

namespace Ow\MarketingApi\Vivo\Report;

use Ow\MarketingApi\Vivo\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{
    /**
     * vivo广告计划
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Vivo\Kernel\Exception\VivoException
     * @param array $parameters
     * @return array|string
     */
    public function campaign(array $parameters = [])
    {
        return $this->request("POST", "ad/campaign/pageInfo", $parameters);
    }

    /**
     * vivo广告组
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Vivo\Kernel\Exception\VivoException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function group(array $parameters = [])
    {
        return $this->request("POST", "ad/group/pageInfo", $parameters);
    }

    /**
     * vivo广告
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Vivo\Kernel\Exception\VivoException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function advertisement(array $parameters = [])
    {
        return $this->request("POST", "ad/advertisement/pageInfo", $parameters);
    }
}