<?php

namespace Ow\MarketingApi\Vivo\Kernel\Cache;

class Cache
{
    protected $client;

    /**
     * 写入缓存
     * @param $key
     * @param $value
     * @param int $time
     * @return bool|mixed
     */
    public function set(string $key, $value, int $time = null)
    {
        if (empty($key) || empty($value)) {
            return false;
        }

        // 如果传入的是数组，就编码下
        $value = is_array($value) ? json_encode($value,320) : $value;

        if($time) {
            return $this->_setex($key, $time, $value);
        }

        return $this->client->set($key, $value);
    }

    /**
     * 获取缓存
     * @param string $key 缓存标识
     * @return bool
     */
    public function get(string $key)
    {
        if (empty($key)) {
            return false;
        }

        return $this->client->get($key);
    }

    /**
     * 删除缓存
     * @param $key
     * @return bool
     */
    public function del($key)
    {
        if (empty($key)) {
            return false;
        }

        return $this->client->del($key);
    }

    /**
     * 设置以秒为过期时间单位的缓存
     * @param string      $key    键
     * @param int         $time   时间
     * @param string      $value  值
     * @return bool|mixed
     */
    public function _setex(string $key, int $time, string $value)
    {
        return $this->client->setex($key, $time, $value);
    }

    /**
     * hash
     * 如果哈希表不存在，一个新的哈希表被创建并进行 HSET 操作
     * 如果字段已经存在于哈希表中，旧值将被覆盖
     * @param string $key    键
     * @param string $field  字段
     * @param string $value  值
     * @return bool
     */
    public function hset(string $key, string $field, string $value)
    {
        if (empty($key) || empty($field) || empty($value)) {
            return false;
        }

        return $this->client->hset($key, $field, $value);
    }

    /**
     * hash
     * 获取哈希表某个字段值
     * @param string $key
     * @param string $field
     * @return bool
     */
    public function hget(string $key, string $field)
    {
        if (empty($key) || empty($field)) {
            return false;
        }

        return $this->client->hget($key, $field);
    }

    /**
     * 哈希表某个值累加，没有就创建
     * @param string $key
     * @param string $field
     * @param string $value
     * @return bool
     */
    public function hIncrBy(string $key, string $field, string $value)
    {
        if (empty($key) || empty($field) || empty($value)) {
            return false;
        }

        return $this->client->hIncrBy($key, $field, $value);
    }

    /**
     * hash
     * 获取哈希表所有字段
     * @param string $key
     * @return bool
     */
    public function hgetall(string $key)
    {
        if (empty($key)) {
            return false;
        }

        return $this->client->hgetall($key);
    }

    /**
     * 获取hash表长度
     * @param string $key
     * @return bool
     */
    public function hlen(string $key)
    {
        if (empty($key)) {
            return false;
        }

        return $this->client->hlen($key);
    }

    /**
     * 将一个或多个值插入到列表头部。
     * 如果 key 不存在，一个空列表会被创建并执行 LPUSH 操作。 当 key 存在但不是列表类型时，返回一个错误
     * @param string $key
     * @param $value
     * @return bool
     */
    public function lpush(string $key, $value)
    {
        if (empty($key) || empty($value)) {
            return false;
        }
        // 如果传入的是数组，就编码下
        $value = is_array($value) ? json_encode($value,320) : $value;

        return $this->client->lpush($key, $value);
    }

    /**
     * Redis Lpop 命令用于移除并返回列表的第一个元素
     * @param string $key
     * @return bool
     */
    public function lpop(string $key)
    {
        if (empty($key)) {
            return false;
        }

        return $this->client->lpop($key);
    }

    /**
     * Redis Rpush 命令用于将一个或多个值插入到列表的尾部(最右边)
     * @param string $key
     * @param $value
     * @return bool
     */
    public function rpush(string $key, $value)
    {
        if (empty($key) || empty($value)) {
            return false;
        }

        // 如果传入的是数组，就编码下
        $value = is_array($value) ? json_encode($value,320) : $value;

        return $this->client->rpush($key, $value);
    }

    /**
     * Redis Rpop 命令用于移除并返回列表的最后一个元素
     * @param string $key
     * @return bool
     */
    public function rpop(string $key)
    {
        if (empty($key)) {
            return false;
        }

        return $this->client->rpop($key);
    }

    /**
     * 获取list列表长度
     * @param string $key
     * @return bool
     */
    public function llen(string $key)
    {
        if (empty($key)) {
            return false;
        }

        return $this->client->llen($key);
    }

    /**
     * 返回列表中指定区间内的元素，区间以偏移量 START 和 END 指定
     * 0 表示列表的第一个元素， 1 表示列表的第二个元素
     * 以 -1 表示列表的最后一个元素， -2 表示列表的倒数第二个元素
     * @param string $key
     * @param int $start
     * @param $end
     * @return bool
     */
    public function lrange(string $key, int $start, $end)
    {
        if (empty($key)) {
            return false;
        }

        return $this->client->lrange($key, $start, $end);
    }

    /**
     * hash多字段新增
     * @param string $key
     * @param array $array
     * @return bool
     */
    public function hmSet(string $key, array $array)
    {
        if (!is_array($array)) {
            return false;
        }

        return $this->client->hmset($key, $array);
    }

    /**
     * hash自增float
     * @param string $key
     * @param string $field
     * @param float $value
     * @return mixed
     */
    public function hIncrByFloat(string $key, string $field, float $value)
    {
        return $this->client->hincrbyfloat($key, $field, $value);
    }

    /**
     * cache
     * @param mixed $driver
     */
    public function setDriver($driver): void
    {
        $this->client = $driver;
    }
}
