<?php

namespace Ow\MarketingApi\Vivo\Kernel\Cache;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class CacheServiceProvider
 * @package Ow\MarketingApi\Vivo\Kernel\Cache
 */
class CacheServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple["cache"] = function ($app){
            return new Cache($app);
        };
    }
}
