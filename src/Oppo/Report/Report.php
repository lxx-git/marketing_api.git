<?php

namespace Ow\MarketingApi\Oppo\Report;

use Ow\MarketingApi\Oppo\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{
    /**
     * oppo广告计划报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Oppo\Kernel\Exception\OppoException
     * @param array $parameters
     * @return array|string
     */
    public function plan(array $parameters = [])
    {
        return $this->request("POST", "plan/page", $parameters);
    }

    /**
     * oppo广告组报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Oppo\Kernel\Exception\OppoException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function group(array $parameters = [])
    {
        return $this->request("POST", "group/pageInfo", $parameters);
    }
}