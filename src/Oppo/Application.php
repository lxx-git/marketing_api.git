<?php

namespace Ow\MarketingApi\Oppo;

use Ow\MarketingApi\Oppo\Kernel\ServiceContainer;
use Ow\MarketingApi\Oppo\Report\ReportServiceProvider;

/**
 * Class Application
 * @property \Ow\MarketingApi\Oppo\Report\Report $report
 * @property \Ow\MarketingApi\Oppo\Kernel\Cache\Cache $cache
 * @package Ow\MarketingApi\Oppo
 */
class Application extends ServiceContainer
{
    protected $providers = [
        ReportServiceProvider::class
    ];
}
